import Difference
import Logistic

defmodule Bifurcation do
    @moduledoc """
        Calculate all the bifurcations of a 1d map when its parameter is varied.
    """   
    
    @doc """
    Find the parameter range where a periodic motion of a given order exists.
    """
    def get_parameters_period(order) do
        cycles = Enum.map(2..order, &find_cycle/1)

        for i <- 1..(order-1) do
            [head|_tail] = Enum.take(Enum.scan(Enum.reverse(Enum.take(cycles, i)), fn a,b -> b -- a end), -1)
            intervals(Enum.filter(split(head), fn x-> x != [] end))
        end
    end

    def find_cycle(order) do
	    interval = 30000..40000 |> Enum.map(&(&1*0.0001))       
        cycle? = &has_cycle?(0.3, &1, order, 12000, 1000)	

        Enum.zip(interval, interval |> Enum.map(cycle?)) |> 
        Enum.filter(&(elem(&1, 1) == true)) |> 
        Enum.map(&(elem(&1,0)))
    end

    def has_cycle?(x, r, order, n, m) do
    	orbit = Enum.take(iterate(x, r, n), -m)
        p = diff(Enum.take_every(orbit, order))
	    Enum.all?(p, fn a -> a < 0.001 end)
    end

    @doc """
    Given a list of lists, returns the min and max of each list.
    """
    defp intervals(a) when length(a) >= 1 do
        [head |tail] = a
        [{Enum.min(head), Enum.max(head)}] ++ intervals(tail)
    end

    defp intervals(a) when length(a) == 0 do
        []
    end

    defp split(a) when length(a) > 0 do
        head = Enum.map(Enum.take_while(Enum.zip(a, diff(a)), &(abs(elem(&1,1))<=0.003)), &(elem(&1,0)))
        [_x|tail] = a -- head

        if length(head) != length(a)-1, do: [head] ++ split(tail), else: [head] 
    end

    defp split(a) when length(a) == 0 do
        []
    end
end
