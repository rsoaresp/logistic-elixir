defmodule Difference do
    @moduledoc """
    Auxiliary functions.
    """
    
    def diff(orbit) when length(orbit) > 1 do
    	[first, second | tail] = orbit
        [second - first | diff([second | tail])]
    end
    
    def diff(orbit) when length(orbit) == 1 do
	    []
    end
end