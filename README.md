# Logistic Elixir

This is a really small project that I developed such that I could learn the very basics of the [Elixir](https://elixir-lang.org/) programming language. By its nature, of a small fist project at a new language, it suffers from many pathologies. Don't expect it to be the best implementation possible. However, it may be helpful to others interested in learning this cool language!

## What is does?

The dynamics of the [logistic map](https://mathworld.wolfram.com/LogisticMap.html) is governed by a one dimensional map that is controlled by a single parameter. This code calculates for which values of this parameter bifurcations happens: the birth of motion that is periodic with period n.

For example, by calling

`
Bifurcation.get_parameters_period(5)
`

```
[
  [{3.0, 3.4493}],
  [{3.8285, 3.8414}],
  [{3.4495, 3.544}, {3.9602000000000004, 3.9614000000000003}],
  [{3.7382, 3.7412}, {3.9056, 3.9061000000000003}]
]
```

This tells us that a periodic motion of period 2 exists between the values of (3, 3.4493), period 4 bifurcates from it at r=3.4495, with a small island at r=3.9602. The odd periodic motions of periods 3 and 5 have windows beginning at 3.8285 and 3.7382, respectively.


## How to build it?

Create the binaries of each file by calling the `elixirc` command. 

## Next steps?

Refactor the functions at the Bifurcation.ex file. They became complicated and can be broken in smaller and with more defined responsabilities, as a functional code should looks like. Furthermore, add some unit tests.
