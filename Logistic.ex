defmodule Logistic do
    @moduledoc """
    This module defines the logistic equation.
    """
    
    def iterate(x, r, n) do
	    map([x], r, n)
    end

    defp map(x, r, n) when n > 0 do
	    [head | _] = x
	    map([logistic(r, head) | x], r, n-1)
    end

    defp map(x, _r, n) when n == 0 do
	    Enum.reverse(x)
    end

    defp logistic(r, x) do
	    r*x*(1.0 - x)
    end
end